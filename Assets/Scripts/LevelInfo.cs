﻿using UnityEngine;
using System.Collections;

public class LevelInfo : MonoBehaviour {

    private const int MAX_CARDS_NUMBER = 11;

    string _levelName;
    public int _levelID;
    private int neededPaws = 0;
    bool playable = false;

    LevelRules rules;

    public void Init(int levelID)
    {
        transform.GetComponentInChildren<UILabel>().text = (levelID + 1).ToString();
        _levelID = levelID;
        _levelName = "L" + _levelID;

        int maxClicksAllowed;
        int idealClicksAllowed;
        int idealTimeToFinish, maxTimeToFinish;
        float timeBefor;

        var strPrefix = "Sokoban/" + MainMenuController.Instance.GameController.EpisodeName + "/Levels/" + _levelName;
        /*
         * <idealClicks>30</idealClicks>
           <maxClicks>32</maxClicks>
           <idealTime>40</idealTime>
           <maxTime>42</maxTime>
         */
        int.TryParse(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/maxClicks")[0].InnerText, out maxClicksAllowed);
        int.TryParse(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/idealClicks")[0].InnerText, out idealClicksAllowed);
        int.TryParse(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/idealTime")[0].InnerText, out idealTimeToFinish);
        int.TryParse(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/maxTime")[0].InnerText, out maxTimeToFinish);

        float.TryParse(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/timeBefor")[0].InnerText, out timeBefor);

        var neededPawsStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/neededPaws");
        if (neededPawsStr.Count > 0)
        {
            int.TryParse(neededPawsStr[0].InnerText, out neededPaws);           
        }

        int up = 0;
        int down = 0; 
        int left = 0;
        int right = 0;

        if(MainMenuController.LoadedXML.SelectNodes(strPrefix + "/moves").Count > 0){
            var upStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/moves/up")[0].InnerText;
            int.TryParse(upStr, out up);

            var downStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/moves/down")[0].InnerText;
            int.TryParse(downStr, out down);

            var leftStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/moves/left")[0].InnerText;
            int.TryParse(leftStr, out left);

            var rightStr = MainMenuController.LoadedXML.SelectNodes(strPrefix + "/moves/right")[0].InnerText;
            int.TryParse(rightStr, out right);
        }

        rules = new LevelRules(idealTimeToFinish, maxTimeToFinish,
                               idealClicksAllowed, maxClicksAllowed,
                               timeBefor,
                               up, down, left, right,
                               MainMenuController.Instance.GameController.EpisodeName.EndsWith("3"));

        string levelStatus = SetLevelImageName(_levelID, rules);

        var level = transform.GetComponent<UISprite>();
        level.spriteName = levelStatus;
    }

    public void Reinit() 
    {
        string levelStatus = SetLevelImageName(_levelID, rules);

        var level = transform.GetComponent<UISprite>();
        level.spriteName = levelStatus;
    }

    private string SetLevelImageName(int levelID, LevelRules rules) 
    {
        string levelName = "L" + levelID;

        string result = "playbtn_dissabled";
        string clicks = "", time = "";
        ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.CLICKS, levelName, out clicks);
        ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.TIME, levelName, out time);
        if (!clicks.Equals("") && !time.Equals(""))
        {
            int cl, amountOfStars;
            float t;
            if (int.TryParse(clicks, out cl))
                if (float.TryParse(time, out t))
                {
                    amountOfStars = 1;
                    if (cl < rules.idealClicksAllowed) { amountOfStars++; }
                    if (t < rules.idealTimeToFinish) { amountOfStars++; }

                    result = "playbtn" + amountOfStars;

                    playable = true;
                }
        }
        else
        {
            ProgressManager.Instance.GetCurrentEpisodeData(ProgressManager.CLICKS, "L" + (_levelID-1), out clicks);
            if ((!clicks.Equals("") && neededPaws <= PlayerPrefs.GetInt("Score", 0)) || _levelID == 0)
            {
                result = "playbtn0";
                playable = true;
            }
        }

        return result;
    }

    public void OnClick() 
    {
        if (!playable)
            return;

        MainMenuController.Instance.GameController.LevelName = _levelName;
        MainMenuController.Instance.GameController.LevelId = _levelID;

        int x,y;
        var node = MainMenuController.LoadedXML.SelectNodes("Sokoban/" + MainMenuController.Instance.GameController.EpisodeName + "/Levels/" + _levelName)[0];
        int.TryParse(node.SelectNodes("size/x")[0].InnerText, out x);
        int.TryParse(node.SelectNodes("size/y")[0].InnerText, out y);

        int countOfCards;
        int.TryParse(node.SelectNodes("imgs")[0].InnerText, out countOfCards);

        MainMenuController.Instance.GameController.SetGameParams(x, y, countOfCards, rules);
        MainMenuController.Instance.GameController.PrepareGame();
        MainMenuController.Instance.GamePanelGO.SetActive(true);
        MainMenuController.Instance.LevelSelectionPanelGO.SetActive(false);
        MainMenuController.Instance.GameController.StartGame();
    }
}
