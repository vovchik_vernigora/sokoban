﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSelection : MonoBehaviour {

    public static LevelSelection Instance;

    public GameObject LevelCardPrefab;

    private float levelCardHeight;
    private float levelCardWidth;

    private Transform cardsHolder;

    private GameObject nextBtn;
    private GameObject prevBtn;

    private int curCardsPos;
    private int maxCardsPos;

    private float delta;
    private Vector3 wantedPos;

    private UILabel scoreLabel;
    private UILabel totalScoreLabel;

    private UIToggle[] toggleHolder;

    private Vector2 startPos;
    private Vector2 endPos;
    private bool swiped;
    private float moveDelta;

    string curEpisodeName = "";
    public List<LevelInfo> curLevels;

    void Awake() 
    {
        Instance = this;

        int height = 2;
        int width = 5;
        levelCardHeight = Screen.height * ((100f - PublicVars.menuBorderGap * 2 - ((float)height - 1) * PublicVars.menuCardsGap) / (float)height / 100f); // 10% gap from all borders. and 5% gap between cards
        levelCardWidth = Screen.width * ((100f - PublicVars.menuBorderGap * 2 - ((float)width - 1) * PublicVars.menuCardsGap) / (float)width / 100f); // 10% gap from all borders. and 5% gap between cards

        cardsHolder = transform.FindChild("CardsHolder");
        curCardsPos = 0;
        nextBtn = transform.FindChild("NextCards").gameObject;
        prevBtn = transform.FindChild("PrevCards").gameObject;
        delta = 1;
        UpdateVisibility();

        scoreLabel = transform.FindChild("Score").GetComponent<UILabel>();
        totalScoreLabel = transform.FindChild("totalScoreLabel").GetComponent<UILabel>();
        toggleHolder = transform.FindChild("toggles").GetComponentsInChildren<UIToggle>();
    }

    public void UpdateScoresLabel() 
    {
        scoreLabel.text = PlayerPrefs.GetInt(ProgressManager.SCORE, 0) + "/90";
        totalScoreLabel.text = PlayerPrefs.GetInt(ProgressManager.TOTAL_SCORE, 0).ToString();
    }

    public void ReInitLevels() 
    {
        InitLevels(curEpisodeName, curLevels.Count);
    }

    public void InitLevels(string episodeName, int count) 
    {
        if (!curEpisodeName.Equals(episodeName))
        {
            foreach (var levelCard in curLevels)
            {
                Destroy(levelCard.gameObject);
            }

            maxCardsPos = (count / PublicVars.cardsPerScreenInMenu);
            curEpisodeName = episodeName;
            curLevels = new List<LevelInfo>();
            for (int i = 0; i < maxCardsPos; i++)
            {
                for (int j = 0; j < PublicVars.cardsPerScreenInMenu; j++)
                {
                    var card = (GameObject)Instantiate(LevelCardPrefab);

                    var component = card.GetComponent<LevelInfo>();
                    component.Init(PublicVars.cardsPerScreenInMenu * i + j);
                    curLevels.Add(component);

                    var cTrans = card.transform;
                    cTrans.parent = cardsHolder;
                    var widget = card.GetComponent<UIWidget>();

                    var sideSize = levelCardWidth > levelCardHeight ? levelCardHeight : levelCardWidth;

                    widget.width = (int)sideSize;
                    widget.height = (int)sideSize;
                    widget.GetComponent<BoxCollider>().size = new Vector3(sideSize, sideSize);

                    cTrans.localScale = Vector3.one;
                    cTrans.localPosition = new Vector3(Screen.width * PublicVars.menuCardsGapInPercents * ((j % PublicVars.cardsInLineInMenu) + 2) + levelCardWidth * (j % PublicVars.cardsInLineInMenu) + levelCardWidth / 2 - Screen.width / 2 + Screen.width * i,
                                                       Screen.height * PublicVars.menuCardsGapInPercents * ((j / PublicVars.cardsInLineInMenu) + 2) + levelCardHeight * (1 - j / PublicVars.cardsInLineInMenu) + levelCardHeight / 2 - Screen.height / 2);
                }
            }
        }
        else 
        {
            foreach (var levelCard in curLevels)
            {
                levelCard.Reinit();
            }
        }
        UpdateScoresLabel();
    }

    public void ShowNextCards() 
    {
        curCardsPos++;
        if (curCardsPos >= maxCardsPos) curCardsPos = maxCardsPos - 1;
        UpdateCardsHolder();
    }

    public void ShowPrevCards() 
    {
        curCardsPos--;
        if (curCardsPos <= 0) curCardsPos = 0;
        UpdateCardsHolder();
    }

    private void UpdateCardsHolder() 
    {
        delta = 0;
        wantedPos = curCardsPos * Vector3.left * Screen.width;
        toggleHolder[curCardsPos].value = true;
        UpdateVisibility();
    }

    private void UpdateVisibility() 
    {
        if (curCardsPos == 0) 
        {
            prevBtn.SetActive(false);
            nextBtn.SetActive(true);
        }
        else if (curCardsPos == maxCardsPos - 1)
        {
            prevBtn.SetActive(true);
            nextBtn.SetActive(false);
        }
        else 
        {
            prevBtn.SetActive(true);
            nextBtn.SetActive(true);
        }
    }

    public void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
        ReInitLevels();
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    swiped = false;
                    startPos = touch.position;
                    break;
                case TouchPhase.Moved:
                    moveDelta = (startPos.x - touch.position.x);
                    cardsHolder.localPosition = new Vector3(-curCardsPos * Screen.width - moveDelta, cardsHolder.localPosition.y);
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    endPos = touch.position;
                    swiped = true;
                    break;
                default: break;
            }
            if (swiped)
            {
                float deltaSwype = endPos.x - startPos.x;
                Debug.LogError(deltaSwype);
                if (deltaSwype < -(Screen.width/3)) // swipped enought for next 10 cards
                {
                    ShowNextCards();
                }
                else if (deltaSwype > (Screen.width / 3)) // swipped enought for prev 10 cards
                {
                    ShowPrevCards();
                }
                else //not swiped enought 
                {
                    UpdateCardsHolder();
                }
                swiped = false;
            }
        }
        else if (delta < 1)
        {
            cardsHolder.localPosition = Vector3.Lerp(cardsHolder.localPosition, wantedPos, delta);
            delta += Time.deltaTime;
        }
    }
}
