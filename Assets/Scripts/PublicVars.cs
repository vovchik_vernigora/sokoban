﻿using UnityEngine;
using System.Collections;

public class PublicVars {
    public const float gameCardsGap = 2;
    public const float gameCardsGapInPercents = gameCardsGap / 100;

    public const float gameBorderGap = 10;
    public const float gameBorderGapInPercents = gameBorderGap / 100;

    public const float menuCardsGap = 5;
    public const float menuCardsGapInPercents = menuCardsGap / 100;

    public const float menuBorderGap = 10;
    public const float menuBorderGapInPercents = menuBorderGap / 100;

    public const int cardsInLineInMenu = 5;
    public const int cardsPerScreenInMenu = 10;

    public const int MUTE_ON = 0;
    public const int MUTE_OFF = 1;

    public const float TIME_TO_FLIP_CARDS_IN_GAME = 0.2f;

    public const float TIME_TO_SHOW_EMPTY_SCREEN = 0.5f;
}
