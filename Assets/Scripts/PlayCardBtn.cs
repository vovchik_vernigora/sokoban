﻿using UnityEngine;
using System.Collections;

public class PlayCardBtn : MonoBehaviour
{
    public string SpriteName;
    public int Number;

    public GameController.CardMove OverJump;

    private GameController.CardMove _cardMoveStatus;
    public GameController.CardMove CardMoveStatus
    {
        get 
        {
            return _cardMoveStatus;
        }
        set 
        {
            _cardMoveStatus = value;
            if (value != GameController.CardMove.NONE)
            {
                directionMoveImg.spriteName = "game_card_arrow_" + value.ToString().ToLower();
            }
        }
    }

    public int rowId;
    public int columnId;

    public bool willBeDeleted;

    private UISprite uiSprite;
    private UISprite bgSprite;
    private UISprite directionMoveImg;
    private GameController _gameController;
    private Transform _tr;

    private const string BLANK = "back";
    private const string NORMAL = "front";
    private const string EMPTY = "";

    private bool notifyAboutTheEndOfMove;

    private float delta = 1.1f;
    private Vector3 prevPos;
    private Vector3 newPos;

    private Vector3 overJumpPosShowToHide;
    private Vector3 overJumpPosHideToShow;

    public void Init(GameController gameController, int number,int i, int j, string spriteName)
    {
        _tr = transform;

        this.SpriteName = spriteName;
        _gameController = gameController;

        var card = _tr;
        uiSprite = card.GetComponent<UISprite>();
        uiSprite.spriteName = SpriteName;
        uiSprite.depth = 50;
        this.Number = number;

        this.rowId = i;
        this.columnId = j;

        bgSprite = card.GetChild(0).GetComponent<UISprite>();
        bgSprite.spriteName = NORMAL;
        bgSprite.depth = 49;

        directionMoveImg = card.FindChild("info").GetComponent<UISprite>();
        CardMoveStatus = GameController.CardMove.NONE;
        directionMoveImg.spriteName = " ";
        directionMoveImg.depth = 55;

        OverJump = GameController.CardMove.NONE;
        willBeDeleted = false;
    }

    public void FlipToBack() 
    {
        bgSprite.spriteName = BLANK;
        uiSprite.spriteName = EMPTY;
		directionMoveImg.spriteName = "game_card_arrow_" + _cardMoveStatus.ToString().ToLower();
    }


    void OnPress(bool isPressed) 
    {
        if (isPressed) 
        {
            uiSprite.spriteName = SpriteName;
            bgSprite.spriteName = NORMAL;
            directionMoveImg.spriteName = " ";
            _gameController.OnGameBtnClicked(this);
        }
    }

    /*public void OnClick() {
        uiSprite.spriteName = SpriteName;
        bgSprite.spriteName = NORMAL;
		directionMoveImg.spriteName = " ";
        _gameController.OnGameBtnClicked(this);
    }*/

    public void DoMove(Vector3 newPos, bool notifyAboutTheEndOfMove) 
    {
        this.notifyAboutTheEndOfMove = notifyAboutTheEndOfMove;
        if (_tr != null)
        {
            delta = 0;
            prevPos = _tr.localPosition;
            this.newPos = newPos;
            if (OverJump != GameController.CardMove.NONE) 
            {
                switch (OverJump) 
                {
                    case GameController.CardMove.Down:
                        overJumpPosHideToShow = _tr.localPosition + Vector3.up * Screen.height;
                        overJumpPosShowToHide = _tr.localPosition - Vector3.up * Screen.height;
                        break;
                    case GameController.CardMove.Up:
                        overJumpPosHideToShow = _tr.localPosition - Vector3.up * Screen.height;
                        overJumpPosShowToHide = _tr.localPosition + Vector3.up * Screen.height;
                        break;
                    case GameController.CardMove.Left:
                        overJumpPosHideToShow = _tr.localPosition - Vector3.right * Screen.width;
                        overJumpPosShowToHide = _tr.localPosition + Vector3.right * Screen.width;
                        break;
                    case GameController.CardMove.Right:
                        overJumpPosHideToShow = _tr.localPosition + Vector3.right * Screen.width;
                        overJumpPosShowToHide = _tr.localPosition - Vector3.right * Screen.width;
                        break;
                    default: 
                        break;
                }
            }
        }
    }

    void Update() 
    {
        if (delta < 1.1 && !willBeDeleted)
        {
            if (OverJump == GameController.CardMove.NONE)
            {
                _tr.localPosition = Vector3.Lerp(prevPos, newPos, delta);
            }
            else 
            {
                if (delta < 0.5f) //show to hide
                {
                    _tr.localPosition = Vector3.Lerp(prevPos, overJumpPosShowToHide, delta * 2);
                }
                else // hide to show
                {
                    _tr.localPosition = Vector3.Lerp(overJumpPosHideToShow, newPos, (delta * 2 - 1));
                }
            }
            delta += Time.deltaTime * 2;

            if (delta >= 1.1)
            {
                OverJump = GameController.CardMove.NONE;
                if (notifyAboutTheEndOfMove) 
                {
                    _gameController.MoveFinished(); 
                }
            }
        }
    }

    void OnDestroy()
    {
        if (notifyAboutTheEndOfMove)
        {
            _gameController.MoveFinished();
        }
    }
}
