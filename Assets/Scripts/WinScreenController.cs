﻿using UnityEngine;
using System.Collections;
using System;

public class WinScreenController : MonoBehaviour {

    private UILabel levelNumber;
    private UILabel levelStat;
    private UILabel infoBar;
    private UILabel scoreInfo;
    private Transform pawsBig;
    private Transform pawsSmall;
    private GameObject nextBtn;

    private float delta;
    private int levelScore;

	void Awake () 
    {
        var cachedTr = transform.FindChild("popupBG");
		pawsBig = cachedTr.FindChild("paws_big"); 
		pawsSmall = cachedTr.FindChild("paws_small");
        levelNumber = cachedTr.FindChild("LevelNumber").GetComponent<UILabel>();
        levelStat = cachedTr.FindChild("LevelStat").GetComponent<UILabel>();
        infoBar = cachedTr.FindChild("InfoBar").GetComponent<UILabel>();
        scoreInfo = cachedTr.FindChild("ScoreInfo").GetComponent<UILabel>();

        nextBtn = transform.FindChild("NextBtn").gameObject;
	}

    public void ShowStat() //TODO:refactor this
    {
        if (MainMenuController.Instance.GameController.GamingTime < MainMenuController.Instance.GameController.Rules.maxTimeToFinish &&
            MainMenuController.Instance.GameController.Clicks < MainMenuController.Instance.GameController.Rules.maxClicksAllowed)
        {
            string timeBeforStr;
            ProgressManager.Instance.GetCurrentLevelData(ProgressManager.TIME, out timeBeforStr);
            if (timeBeforStr.Equals("") || float.Parse(timeBeforStr) > MainMenuController.Instance.GameController.GamingTime)
            {
                ProgressManager.Instance.SetCurrentLevelData(ProgressManager.TIME, (MainMenuController.Instance.GameController.GamingTime).ToString());
            }

            string clicksBeforStr;
            ProgressManager.Instance.GetCurrentLevelData(ProgressManager.CLICKS, out clicksBeforStr);
            if (clicksBeforStr.Equals("") || int.Parse(clicksBeforStr) > MainMenuController.Instance.GameController.Clicks)
            {
                ProgressManager.Instance.SetCurrentLevelData(ProgressManager.CLICKS, (MainMenuController.Instance.GameController.Clicks).ToString());
            }
        }

        string curScoreStr;
        int curScore;
        ProgressManager.Instance.GetCurrentLevelData(ProgressManager.SCORE, out curScoreStr);
        if (curScoreStr.Equals(""))
        {
            curScore = 0;
        }
        else  
        {
            curScore = int.Parse(curScoreStr);
        }

        string pawsBeforStr;
        int pawsBefor;
        ProgressManager.Instance.GetCurrentLevelData(ProgressManager.TOTAL_PAWS, out pawsBeforStr);

        if (pawsBeforStr.Equals(""))
        {
            pawsBefor = 0;
        }
        else
        {
            pawsBefor = int.Parse(pawsBeforStr);
        }

        /*
         * score formula score = ((p*10 + t) + n))*100
         * p - pairs in level
         * t - unused clicks
         * n - time left
         *  If  t>tXML and n<nXML then n = n\5
            If  n>nXML and t<tXML then t = t\2
        */
        float p =  MainMenuController.Instance.GameController.playCards / 2;
        float t = MainMenuController.Instance.GameController.Rules.idealClicksAllowed - MainMenuController.Instance.GameController.Clicks;
        float n = MainMenuController.Instance.GameController.Rules.idealTimeToFinish - MainMenuController.Instance.GameController.GamingTime;

        if (t < 0 && n > 0) { n /= 2; p /= 2; }
        if (t > 0 && n < 0) { t /= 2; p /= 2; }
        if (t < 0 && n < 0) { p /= 4; }

		levelScore = (int)(((p*10 + (t<0?0:t)) + (n<0?0:n)) * 100);

        levelNumber.text = MainMenuController.Instance.GameController.EpisodeName + " - " + MainMenuController.Instance.GameController.LevelName;
        levelStat.text = "timing = " + MainMenuController.Instance.GameController.GamingTime + " from " + MainMenuController.Instance.GameController.Rules.idealTimeToFinish;//"??? what to put here??";
        infoBar.text = "Level done. score = " + levelScore;
        scoreInfo.text = "clicks = " + MainMenuController.Instance.GameController.Clicks + " from " + MainMenuController.Instance.GameController.Rules.idealClicksAllowed; //"??? what to put here??";

        int levelScoreInPaws = 0;

        GA.API.Design.NewEvent(MainMenuController.Instance.GameController.EpisodeName + ":level:" + MainMenuController.Instance.GameController.LevelId + ":time",
                                MainMenuController.Instance.GameController.GamingTime);
        GA.API.Design.NewEvent(MainMenuController.Instance.GameController.EpisodeName + ":level:" + MainMenuController.Instance.GameController.LevelId + ":clicks",
                        MainMenuController.Instance.GameController.Clicks);

        GA.API.Error.NewEvent(GA_Error.SeverityType.info, MainMenuController.Instance.GameController.EpisodeName + ":level:" + MainMenuController.Instance.GameController.LevelId + ":time" + MainMenuController.Instance.GameController.GamingTime);
        GA.API.Error.NewEvent(GA_Error.SeverityType.info, MainMenuController.Instance.GameController.EpisodeName + ":level:" + MainMenuController.Instance.GameController.LevelId + ":clicks" + MainMenuController.Instance.GameController.Clicks);

        if (MainMenuController.Instance.GameController.Clicks <= MainMenuController.Instance.GameController.Rules.maxClicksAllowed &&
            MainMenuController.Instance.GameController.GamingTime <= MainMenuController.Instance.GameController.Rules.maxTimeToFinish)
        {
            levelScoreInPaws++;
            nextBtn.SetActive(true); // show next btn
        }
        else 
        {
            levelScoreInPaws = -10;
            nextBtn.SetActive(false); // hide next btn
        }

        if (MainMenuController.Instance.GameController.Clicks <=
            MainMenuController.Instance.GameController.Rules.idealClicksAllowed) 
        {
            levelScoreInPaws++;
        }
        if(MainMenuController.Instance.GameController.GamingTime <= 
            MainMenuController.Instance.GameController.Rules.idealTimeToFinish)
        {
            levelScoreInPaws++;
        }

        if (levelScoreInPaws > pawsBefor)
        {
             ProgressManager.Instance.SetCurrentLevelData(ProgressManager.TOTAL_PAWS, levelScoreInPaws.ToString());

             PlayerPrefs.SetInt(ProgressManager.SCORE, PlayerPrefs.GetInt(ProgressManager.SCORE, 0) + (levelScoreInPaws - pawsBefor));
        }

        int totalScore = PlayerPrefs.GetInt(ProgressManager.TOTAL_SCORE, 0);

        if (levelScore > curScore) 
        {
            PlayerPrefs.SetInt(ProgressManager.TOTAL_SCORE, totalScore - curScore + levelScore);

            ProgressManager.Instance.SetCurrentLevelData(ProgressManager.SCORE, levelScore.ToString());
        }

        infoBar.text += " total = " + PlayerPrefs.GetInt(ProgressManager.TOTAL_SCORE, 0);

        LevelSelection.Instance.UpdateScoresLabel();

        if (levelScoreInPaws < 0) levelScoreInPaws = 0;

        if (levelScoreInPaws < pawsBig.childCount)
        {
            for (int i = 0; i < levelScoreInPaws; i++)
            {
                pawsBig.GetChild(i).gameObject.SetActive(true);
                pawsSmall.GetChild(i).gameObject.SetActive(true);
            }

            for (int i = levelScoreInPaws; i < pawsBig.childCount; i++) //pawsBig.childCount == pawsSmall.childCount == 3 
            {
                pawsBig.GetChild(i).gameObject.SetActive(false);
                pawsSmall.GetChild(i).gameObject.SetActive(false);
            }
        }
        else 
        {
            for (int i = 0; i < pawsBig.childCount; i++)
            {
                pawsBig.GetChild(i).gameObject.SetActive(true);
                pawsSmall.GetChild(i).gameObject.SetActive(true);
            }
        }

        delta = 0;
    }

    void Update() 
    {
        if (delta < 1.1)
        {
            infoBar.text = "Level done. score = " + (int)Mathf.Lerp(0f, levelScore, delta);
            delta += Time.deltaTime;
        }
    }
}
