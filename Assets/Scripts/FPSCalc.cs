﻿using UnityEngine;
using System.Collections;
using System;

public class FPSCalc : MonoBehaviour {

    UILabel label;

	void Start () {
        label = GetComponent<UILabel>();
	}
	
	void Update () {
        label.text = String.Format("FPS:{0,4:N2}", (1 / Time.deltaTime));
	}
}
