﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml;

public class MainMenuController : MonoBehaviour {

    private const string XML_URL = "http://www.scythgames.com/sokoban.xml";

    public static MainMenuController Instance;

    public static XmlDocument LoadedXML;

    public GameObject CardPrefab;

    public GameObject GamePanelGO;
    public GameObject MainMenuGO;
    public GameObject WelcomeSplashGO;
    public GameObject DifficultyPanelGO;
    public GameObject OptionsPanelGO;
    public GameObject LevelSelectionPanelGO;
    public GameObject WinScreenPanelGO;
    public GameObject GamePausePanelGO;

    private UIPanel GameUI;

    public GameController GameController;
    public WinScreenController WinScreenController;

    UILabel developmentInfosLabel;

    void Awake() {

        Instance = this;

        WelcomeSplashGO = GameObject.Find("WelcomeSplash");

        MainMenuGO = GameObject.Find("MainMenu");
        developmentInfosLabel = MainMenuGO.transform.FindChild("Label").GetComponent<UILabel>();

        GamePanelGO = GameObject.Find("GamePanel");
        GameUI = GamePanelGO.GetComponent<UIPanel>();
        GameController = GamePanelGO.GetComponent<GameController>();
        GameController.Init(GameUI);
        GameController.CardPrefab = CardPrefab;

        GameController.GameFinished += OnGameFinished;

        DifficultyPanelGO = GameObject.Find("Difficulty");
        //DifficultyPanel = DifficultyPanelGO.GetComponent<UIPanel>();

        OptionsPanelGO = GameObject.Find("Options");
        LevelSelectionPanelGO = GameObject.Find("LevelSelection");

        WinScreenPanelGO = GameObject.Find("WinScreen");
        WinScreenController = WinScreenPanelGO.GetComponent<WinScreenController>();

        GamePausePanelGO = GameObject.Find("GamePause");

        FB.Init(SetInit);
    }

    void OnGameFinished() 
    {
        WinScreenController.ShowStat();
        WinScreenPanelGO.SetActive(true);
        GamePanelGO.SetActive(false);
    }

	void Start () {

        GamePanelGO.SetActive(false);
        DifficultyPanelGO.SetActive(false);
        OptionsPanelGO.SetActive(false);
        LevelSelectionPanelGO.SetActive(false);
        WinScreenPanelGO.SetActive(false);
        GamePausePanelGO.SetActive(false);

        StartCoroutine(DisableSplashInTime(2));
        StartCoroutine(checkInternetConnection(ConnectionResult));
	}

    void ConnectionResult(bool connected) 
    {
        if (connected)
        {
            StartCoroutine(LoadWWW());
        }
        else 
        {
            developmentInfosLabel.text = "NO INTERNET CONNECTION!!!";
            developmentInfosLabel.color = Color.red;
        }
    }

    IEnumerator checkInternetConnection(System.Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    } 

    IEnumerator LoadWWW() 
    {
        var www = new WWW(XML_URL);
        yield return www;
        var inner = www.text;
        var result = inner;
        LoadedXML = new XmlDocument();
        LoadedXML.LoadXml(result);
    }

    IEnumerator DisableSplashInTime(float time) {
        yield return new WaitForSeconds(time);
        WelcomeSplashGO.SetActive(false);
    }

    public void OnPlayClicked() 
    {
        DifficultyPanelGO.SetActive(true);
        MainMenuGO.SetActive(false);
    }

    public void OnExitClicked()
    {
        Application.Quit();
    }

    public void OnWinScreenMenuClicked()
    {
        WinScreenPanelGO.SetActive(false);
        LevelSelectionPanelGO.SetActive(true);
        LevelSelection.Instance.ReInitLevels();
    }

    public void OnWinScreenReplayClicked()
    {
        GameController.ReplayGame();
    }

    public void OnWinScreenNextClicked()
    {
        WinScreenPanelGO.SetActive(false);
        LevelSelectionPanelGO.SetActive(true);
        LevelSelection.Instance.ReInitLevels();
        foreach (var level in LevelSelection.Instance.curLevels) 
        {
            if (level._levelID == (GameController.LevelId + 1)) 
            {
                level.OnClick();
                break;
            }
        }
    }

    public void OnLevelSelectionBackClicked()
    {
        LevelSelectionPanelGO.SetActive(false);
        DifficultyPanelGO.SetActive(true);
    }

    public void OnDifficultySelectionBackClicked()
    {
        DifficultyPanelGO.SetActive(false);
        MainMenuGO.SetActive(true);
    }

    public void OnSelectLevelClicked()
    {
        GamePanelGO.SetActive(false);
        GameController.ClearCards();
        LevelSelectionPanelGO.SetActive(true);
        GamePausePanelGO.SetActive(false);
    }

    public void OnOptionsClicked()
    {
        OptionsPanelGO.SetActive(true);
        MainMenuGO.SetActive(false);
    }

    public void OnOptionsBackClicked()
    {
        OptionsPanelGO.SetActive(false);
        MainMenuGO.SetActive(true);
    }
    // FB stuff. TODO: move it to new class
    //CAAFdbY5jSnkBAIj3142m2ZBzqtWaLnZAPeDY3mvM2VmtfBCdbenDCDAtihqT3l2LO1QEXZCZCj809z7RIQotvkHCCyjkFfZAHgRS3nGZAtZCxhgYL5ETrsGoZAZARPVELNQvy16zD4YeES7GjqnirhCwlWyk6fUlPtZB6pSdU73y6uC3THXPllEQWsPhpKpU8OIB58X4y3sLI2TFc1agc8zZBz2MsZCn6slCGem8ccvjEfPubTpZC7nzd5xma
    public void FBLogin() 
    {
        FB.Login("email,publish_actions", LoginCallback);
    }

    void LoginCallback(FBResult result)
    {
        if (FB.IsLoggedIn)
        {
            OnLoggedIn();
        }
    }

    void OnLoggedIn()
    {
        developmentInfosLabel.text = "User logged in " + FB.UserId;
        Debug.LogError("FB tocken: " + FB.AccessToken);
    } 

    private void SetInit()
    {
        if (FB.IsLoggedIn)
        {
            OnLoggedIn();
        }
    }

    public void FBPost()
    {
        if (!FB.IsLoggedIn)
        {
            FBLogin();
        }
        else
        {
            FB.Feed(
                    linkCaption: "Kids sokoban",
                    picture: "http://www.scythgames.com/Game_all_08.jpg",
                    linkName: "Checkout my game greatness!",
                    link: "http://apps.facebook.com/" + FB.AppId
                    );
        }
    }
}
