﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour {

    public enum CardMove { Up, Down, Left, Right, NONE};

    public LevelRules Rules;
    public Action GameFinished;

    private UILabel infoLabel;
    public GameObject CardPrefab;
    private UIPanel GameUI;
    private Transform _cardsHolder;

    private UILabel clicksLabel;
    private UILabel timeLabel;

    private int _verticalAmountOfElements;
    private int _horizontalAmountOfElements;

    private float elHeight;
    private float elWidth;

    private bool _gameStarted;
    public float GamingTime;
    public int Clicks;

    public string EpisodeName;
    public string LevelName;
    public int LevelId;

    public int playCards;

    private int _diffCardCount;

    private List<string> cardList;

    private int unicNumber = 0;
    private int offsetInArray;
    private float sideSize;
    private List<PlayCardBtn> cards;

    private List<PlayCardBtn> clickedBtns;

    int _cardsLeft;
    int CardsLeft 
    {
        get { return _cardsLeft; }
        set 
        { 
            _cardsLeft = value;
            if (_cardsLeft == 0) 
            {
                _gameStarted = false;
                StartCoroutine(GameFinishedTimeGap());
            }
        }
    }

    delegate void MoveDelegate(int value);
    private class Moves
    {
        MoveDelegate moveDelegate;
        int columnOrRowId;
        public Moves(MoveDelegate moveDelegate, int columnOrRowId)
        {
            this.moveDelegate = moveDelegate;
            this.columnOrRowId = columnOrRowId;
        }

        public void DoMove()
        {
            moveDelegate(columnOrRowId);
        }
    }

    private Queue<Moves> moveQueue;
    bool inMove;

    private IEnumerator GameFinishedTimeGap() 
    {
        yield return new WaitForSeconds(PublicVars.TIME_TO_SHOW_EMPTY_SCREEN);
        GameFinished();
    }

    public void Init(UIPanel GameUI)
    {
        this.GameUI = GameUI;
        var gameTr = GameUI.transform;

        infoLabel = gameTr.FindChild("InfoLabel").GetComponentInChildren<UILabel>();
        clicksLabel = gameTr.FindChild("clicksLabel").GetComponentInChildren<UILabel>();
        timeLabel = gameTr.FindChild("timeLabel").GetComponentInChildren<UILabel>();

        clickedBtns = new List<PlayCardBtn>();

        _gameStarted = false;
        _cardsHolder = gameTr.FindChild("CardsHolder");
    }

    public void SetGameParams(int height, int width,  int diffCardCount, LevelRules rules) {

        Rules = rules;
        _diffCardCount = diffCardCount;

        elHeight = Screen.height * ((100f - PublicVars.gameBorderGap * 2 - ((float)height - 1) * PublicVars.gameCardsGap) / (float)height / 100f);
        elWidth = Screen.width * ((100f - PublicVars.gameBorderGap * 2 - ((float)width - 1) * PublicVars.gameCardsGap) / (float)width / 100f);

        _verticalAmountOfElements = height;
        _horizontalAmountOfElements = width;

        playCards = height * width;

        cardList = GenerateCardsList(_diffCardCount);
        cards = new List<PlayCardBtn>();

        moveQueue = new Queue<Moves>();
        inMove = false;
    }

    const int MAX_CARD_EMOUNT = 11;
    public List<string> GenerateCardsList(int diffCardCount) 
    {
        int count = playCards / 2;

        var cardNames = new List<int>();
        int n = 0;
        while (n < diffCardCount) 
        {
            int cardId = UnityEngine.Random.Range(1, MAX_CARD_EMOUNT);
            if(!cardNames.Contains(cardId))
            {
                cardNames.Add(cardId);
                n++;
            }
        }

        List<string> result = new List<string>();
        for (int j = 0; j < 2; j++) //2 time add same cards
        {
            for (int i = 0; i < count; i++)
            {
                result.Insert(UnityEngine.Random.Range(0, result.Count), cardNames[i % cardNames.Count].ToString());
            }
        }
        return result;
    }

    public void PrepareGame()
    {
        _cardsLeft = playCards;
        if (cards.Count > 0)
        {
            ClearCards();
            if (_cardsHolder.childCount > 0)
            {
                for (int i = 0; i < _cardsHolder.childCount; i++) 
                {
                    Destroy(_cardsHolder.GetChild(i));
                }
            }
        }

       offsetInArray = 0;
       for (int _rowId = 0; _rowId < _verticalAmountOfElements; _rowId++)
            for (int _columnId = 0; _columnId < _horizontalAmountOfElements; _columnId++)
            {
                var card = (GameObject)Instantiate(CardPrefab);
                var cTrans = card.transform;
                cTrans.parent = _cardsHolder;
                var widget = card.GetComponent<UIWidget>();

                sideSize = elWidth > elHeight ? elHeight : elWidth;

                widget.width = (int)sideSize;
                widget.height = (int)sideSize;
                widget.GetComponent<BoxCollider>().size = new Vector3(sideSize, sideSize);
                var component = card.GetComponent<PlayCardBtn>();
                component.Init(this, unicNumber++, _rowId, _columnId, cardList[offsetInArray]);
                offsetInArray++;
                cards.Add(component);
                cTrans.localScale = Vector3.one;
                cTrans.localPosition = CardPosInScreen(component);
            }

       if (Rules.upMovesCount > 0) 
       {
           GenerateCardMoves(CardMove.Up, Rules.upMovesCount);
       }
       if (Rules.downMovesCount > 0) 
       {
           GenerateCardMoves(CardMove.Down, Rules.downMovesCount);
       }
       if (Rules.leftMovesCount > 0) 
       {
           GenerateCardMoves(CardMove.Left, Rules.leftMovesCount);
       }
       if (Rules.rightMovesCount > 0) 
       {
           GenerateCardMoves(CardMove.Right, Rules.rightMovesCount);
       }
    }

    private void GenerateCardMoves(CardMove moveType, int count) 
    {
        for (int i = 0; i < count; i++)
        {
            cards[UnityEngine.Random.Range(0, cards.Count)].CardMoveStatus = moveType;
        }
    }

    public void ReplayGame() 
    {
        cardList = GenerateCardsList(_diffCardCount);
        PrepareGame();
        MainMenuController.Instance.GamePanelGO.SetActive(true);
        MainMenuController.Instance.WinScreenPanelGO.SetActive(false);
        MainMenuController.Instance.GamePausePanelGO.SetActive(false);
        StartGame();
    }

    public void ClearCards() 
    {
        for (int i = 0; i < cards.Count; i++) 
        {
            if (cards[i] != null && cards[i].gameObject != null)
            {
                Destroy(cards[i].gameObject);
            }
        }
        cards.Clear();
    }

    public void StartGame() 
    {
        StartCoroutine(CountToStartGame());
    }

    private IEnumerator CountToStartGame() 
    {
        float timeLeft = Rules.timeBefor;
        while (timeLeft >= 1)
        {
            infoLabel.text = timeLeft.ToString();
            yield return new WaitForSeconds(1);
            timeLeft -= 1;
        }

        infoLabel.text = "GO!";
        yield return new WaitForSeconds(timeLeft);

        infoLabel.text = "";

        StartGameCountingFinished();
    }

    private void StartGameCountingFinished() 
    {
        HideAllCards();
        GamingTime = 0;
        Clicks = 0;
        _gameStarted = true;
    }

    private void HideAllCards() 
    {
        foreach (var card in cards) 
        {
            card.FlipToBack();
        }
    }

    public void OnGameBtnClicked(PlayCardBtn btn)
    {
        Clicks++;
        clicksLabel.text = ((Clicks < Rules.idealClicksAllowed) ? "[00FF00]" : (Clicks < Rules.maxClicksAllowed ? "[FFFF00]" : "[FF0000]")) + Clicks;
        if (_gameStarted)
        {
            switch (clickedBtns.Count)
            {
                case 0: clickedBtns.Add(btn);
                    break;
                case 1: //one cards showen, second been pressed
                    if (btn.Number != clickedBtns[0].Number) // if the same btn was not pressed
                    {
                        if (clickedBtns[0].SpriteName.Equals(btn.SpriteName)) //another btn with the same anim pressed
                        {
                            clickedBtns.Add(btn);
                            if (CardsLeft > 2)
                            {
                                StartCoroutine(DeleteCardsInTime(clickedBtns.ToArray(), PublicVars.TIME_TO_FLIP_CARDS_IN_GAME));
                            }
                            else
                            {
                                clickedBtns[0].willBeDeleted = true;
                                clickedBtns[1].willBeDeleted = true;
                                Destroy(clickedBtns[0].gameObject);
                                Destroy(clickedBtns[1].gameObject);
                            }
                            clickedBtns.Clear();
                            CardsLeft -= 2;
                        }
                        else // another btn with different anim pressed
                        {
                            clickedBtns.Add(btn);
                            StartCoroutine(FlipCardsInTime(clickedBtns.ToArray(), PublicVars.TIME_TO_FLIP_CARDS_IN_GAME));
                            clickedBtns.Clear();
                        }
                    }
                    else  // the same btn pressed
                    {
                        clickedBtns[0].FlipToBack();
                        clickedBtns.Clear();
                    }
                    break;
                case 2: // 2 cards showen, new was pressed
                    clickedBtns[0].FlipToBack();
                    clickedBtns[1].FlipToBack();
                    clickedBtns.Clear();
                    clickedBtns.Add(btn);
                    break;
                default: break;
            }
        }
    }

    protected IEnumerator FlipCardsInTime(PlayCardBtn[] btns, float time) 
    {
        yield return new WaitForSeconds(time);
        btns[0].FlipToBack();
        btns[1].FlipToBack();
    }

    protected IEnumerator DeleteCardsInTime(PlayCardBtn[] btns, float time)
    {
        clickedBtns[0].willBeDeleted = true;
        clickedBtns[1].willBeDeleted = true;
        cards.Remove(clickedBtns[0]);
        cards.Remove(clickedBtns[1]);
        DoCardMovements(btns[0]);
        DoCardMovements(btns[1]);
        yield return new WaitForSeconds(time);

        Destroy(btns[0].gameObject);
        Destroy(btns[1].gameObject);

        if (Rules.userGravity)
        {
            AddMove(new Moves(UpgradeGravityMove, 0)); // gravity works for all cards
        }
    }

    protected void DoCardMovements(PlayCardBtn btn)
    {
        switch (btn.CardMoveStatus) 
        {
            case CardMove.Up:
                            AddMove(new Moves(MoveCardsUpByColumnId, btn.columnId));
                            break;
            case CardMove.Down:
                            AddMove(new Moves(MoveCardsDownByColumnId, btn.columnId));
                            break;
            case CardMove.Left:
                            AddMove(new Moves(MoveCardsLeftByRowId, btn.rowId));
                            break;
            case CardMove.Right:
                            AddMove(new Moves(MoveCardsRightByRowId, btn.rowId));
                            break;
            case CardMove.NONE:
            default: break;
        }
    }

    private void DoNextMove() 
    {
        if (!inMove)
        {
            inMove = true;
            moveQueue.Dequeue().DoMove();
        }
    }

    private void UpgradeGravityMove(int stub) // gravity works for all cards
    {
        UpdateGravity();
    }
    
    private void UpdateGravity() 
    {
        UpdateCardsPosAccordingGravity();
    }

    private void AddMove(Moves nextMove) 
    {
        moveQueue.Enqueue(nextMove);
        DoNextMove();
    }

    public void MoveFinished() 
    {
        inMove = false;
        if (moveQueue.Count > 0)
        {
            DoNextMove();
        }
    }

    public void PauseBtnClicked() 
    {
        _gameStarted = false;
        MainMenuController.Instance.GamePausePanelGO.SetActive(true);
    }

    protected void MoveCardsDownByColumnId(int columnId)
    {
        List<PlayCardBtn> cardsToMove = GetCardsByColumnId(columnId);
        bool nottifierBtn = true;
        foreach (var card in cardsToMove)
        {
            int _rowId;
            if (card.rowId - 1 < 0) 
            { 
                _rowId = _verticalAmountOfElements - 1;
                card.OverJump = CardMove.Down;
            }
            else
            { 
                _rowId = card.rowId - 1; 
            }
            card.rowId = _rowId;

            card.DoMove(CardPosInScreen(card), nottifierBtn);
            nottifierBtn = false;
        }
    }

    protected void MoveCardsUpByColumnId(int columnId) 
    {
        List<PlayCardBtn> cardsToMove = GetCardsByColumnId(columnId);
        bool nottifierBtn = true;
        foreach (var card in cardsToMove)
        {
            int _rowId;
            if (card.rowId + 1 > _verticalAmountOfElements - 1) 
            { 
                _rowId = 0;
                card.OverJump = CardMove.Up; 
            } 
            else 
            { 
                _rowId = card.rowId + 1; 
            }
            card.rowId = _rowId;

            card.DoMove(CardPosInScreen(card), nottifierBtn);
            nottifierBtn = false;
        }
    }

    protected void MoveCardsLeftByRowId(int rowId) 
    {
        List<PlayCardBtn> cardsToMove = GetCardsByRowId(rowId);
        bool nottifierBtn = true;
        foreach (var card in cardsToMove)
        {
            int _columnId;
            if (card.columnId - 1 < 0) 
            {
                _columnId = _horizontalAmountOfElements - 1; 
                card.OverJump = CardMove.Right; 
            } 
            else 
            {
                _columnId = card.columnId - 1; 
            }
            card.columnId = _columnId;

            card.DoMove(CardPosInScreen(card), nottifierBtn);
            nottifierBtn = false;
        }
    }

    protected void MoveCardsRightByRowId(int rowId)
    {
        List<PlayCardBtn> cardsToMove = GetCardsByRowId(rowId);
        bool nottifierBtn = true;
        foreach (var card in cardsToMove)
        {
            int _cloumnId;
            if (card.columnId + 1 > _horizontalAmountOfElements - 1) 
            {
                _cloumnId = 0; 
                card.OverJump = CardMove.Left; 
            } 
            else 
            { 
                _cloumnId = card.columnId + 1; 
            }
            card.columnId = _cloumnId;

            card.DoMove(CardPosInScreen(card), nottifierBtn);
            nottifierBtn = false;
        }
    }

    public List<PlayCardBtn> GetCardsByColumnId(int columnId)
    {
        var result = new List<PlayCardBtn>();
        foreach (var card in cards)
        {
            if (card.columnId == columnId)
            {
                result.Add(card);
            }
        }

        return result;
    }

    public List<PlayCardBtn> GetCardsByRowId(int rowId)
    {
        var result = new List<PlayCardBtn>();
         foreach (var card in cards)
        {
            if (card.rowId == rowId)
            {
                result.Add(card);
            }
        }

        return result;
    }

    public void ResumeBtnClicked()
    {
        _gameStarted = true;
        MainMenuController.Instance.GamePausePanelGO.SetActive(false);
    }

    public void MuteBtnPressed() 
    {
        if (NGUITools.soundVolume > 0.5f)
        {
            NGUITools.soundVolume = PublicVars.MUTE_ON;
            Camera.main.GetComponent<AudioSource>().volume = PublicVars.MUTE_ON;
        }
        else 
        {
            NGUITools.soundVolume = PublicVars.MUTE_OFF;
            Camera.main.GetComponent<AudioSource>().volume = PublicVars.MUTE_OFF;
        }
    }

    protected void UpdateCardsPosAccordingGravity() 
    {
        bool nottifierBtn = true;
        for (int column = 0; column < _horizontalAmountOfElements; column++)
        {
            var cardsToMove = GetCardsByColumnId(column);
            cardsToMove.Sort(delegate(PlayCardBtn x, PlayCardBtn y)
            {
                if (x.rowId > y.rowId) return 1;
                else if (x.rowId < y.rowId) return -1;
                else return 0;
            });

            for(int row = 0; row < cardsToMove.Count; row++)
            {
                cardsToMove[row].rowId = row;
                cardsToMove[row].DoMove(CardPosInScreen(cardsToMove[row]), nottifierBtn);
                nottifierBtn = false;
            }
        }
    }

    public Vector3 CardPosInScreen(PlayCardBtn card) 
    {
        return new Vector3(Screen.width * PublicVars.gameBorderGapInPercents + (elWidth - sideSize) * _horizontalAmountOfElements / 2 + Screen.height * PublicVars.gameCardsGapInPercents * card.columnId + sideSize * card.columnId + sideSize / 2 - Screen.width / 2,
                                                Screen.height * PublicVars.gameBorderGapInPercents + Screen.height * PublicVars.gameCardsGapInPercents * card.rowId + sideSize * card.rowId + sideSize / 2 - Screen.height / 2);
    }

    void Update()
    {
        if (_gameStarted)
        {
            GamingTime += Time.deltaTime;
            timeLabel.text = ((GamingTime < Rules.idealTimeToFinish) ? "[00FF00]" : (GamingTime < Rules.maxTimeToFinish ? "[FFFF00]" : "[FF0000]")) + GamingTime;//GamingTime + " / " + Rules.idealTimeToFinish + " / " + Rules.maxTimeToFinish;
        }
    }
}
