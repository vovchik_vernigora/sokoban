﻿using UnityEngine;
using System.Collections;

public class DiffidultyPanel : MonoBehaviour {

    public static DiffidultyPanel Instance;

    private const int episodeImgWidth = 604;
    private const int episodeImgHeight = 836;

    void Awake() 
    {
        Instance = this;

        int imgHeight = (int)(Screen.height * 0.55f);
        int imgWidth = (int)(episodeImgWidth * ((float)imgHeight / (float)episodeImgHeight));

        var ep1Tr = transform.FindChild("episode1");
        var ep1Sprite = ep1Tr.GetChild(0).GetComponent<UISprite>();
        ep1Sprite.height = imgHeight;
        ep1Sprite.width = imgWidth;
        ep1Tr.localPosition = new Vector3(-Screen.width / 4,0);
        var ep1BoxCollider = ep1Tr.GetComponent<BoxCollider>();
        ep1BoxCollider.size = new Vector3(imgHeight, imgWidth);

        var ep2Tr = transform.FindChild("episode2");
        var ep2Sprite = ep2Tr.GetChild(0).GetComponent<UISprite>();
        ep2Sprite.height = imgHeight;
        ep2Sprite.width = imgWidth;
        ep2Tr.localPosition = new Vector3(0, 0);

        var ep3Tr = transform.FindChild("episode3");
        var ep3Sprite = ep3Tr.GetChild(0).GetComponent<UISprite>();
        ep3Sprite.height = imgHeight;
        ep3Sprite.width = imgWidth;
        ep3Tr.localPosition = new Vector3(Screen.width / 4, 0);
    }

    public void PlayEpisode1() 
    {
        StartEpisodeById(1);
    }

    public void PlayEpisode2() 
    {
        StartEpisodeById(2);
    }

    public void PlayEpisode3() 
    {
        StartEpisodeById(3);
    }

    private void StartEpisodeById(int id) 
    {
        MainMenuController.Instance.DifficultyPanelGO.SetActive(false);
        MainMenuController.Instance.LevelSelectionPanelGO.SetActive(true);
        int count = MainMenuController.LoadedXML.SelectNodes("Sokoban/Episode" + id + "/Levels/*").Count;

        MainMenuController.Instance.GameController.EpisodeName = "Episode" + id;

        LevelSelection.Instance.InitLevels("Episode" + id, count);
    }
}
