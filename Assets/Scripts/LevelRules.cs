﻿public class LevelRules
{
    public int idealTimeToFinish;
    public int maxTimeToFinish;
    public int idealClicksAllowed;
    public int maxClicksAllowed;
    public float timeBefor;

    public int upMovesCount,
               downMovesCount,
               leftMovesCount,
               rightMovesCount;

    public bool userGravity;

    public LevelRules(int idealTimeToFinish, int maxTimeToFinish, int idealClicksAllowed, int maxClicksAllowed, float timeBefor, int upMovesCount, int downMovesCount, int leftMovesCount, int rightMovesCount, bool userGravity) 
    {
        this.idealTimeToFinish = idealTimeToFinish;
        this.maxTimeToFinish = maxTimeToFinish;
        this.idealClicksAllowed = idealClicksAllowed;
        this.maxClicksAllowed = maxClicksAllowed;
        this.timeBefor = timeBefor;

        this.upMovesCount = upMovesCount;
        this.downMovesCount = downMovesCount;
        this.leftMovesCount = leftMovesCount;
        this.rightMovesCount = rightMovesCount;
        this.userGravity = userGravity;
    }
}
