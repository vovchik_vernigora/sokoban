﻿using UnityEngine;
using System.Collections;

public class ProgressManager : MonoBehaviour {

    public const string TIME = "time";
    public const string CLICKS = "clicks";
    public const string FINISHED = "finished";
    public const string TOTAL_PAWS = "total";
    public const string TOTAL_SCORE = "totalScore";
    public const string SCORE = "Score";

    private const int notfinished = 0;
    private const int finished = 1;

    public static ProgressManager Instance;

	void Awake () {
        Instance = this;
	}

    public void GetData(string key, out string result) 
    {
        result = PlayerPrefs.GetString(key, null);
    }

    public void SetData(string key, string data) 
    {
        PlayerPrefs.SetString(key, data);
    }

    public bool IsPrevLevelFinishedData(string currentLevel)
    {
        int levelNumer;
        int.TryParse(currentLevel.Substring(1),out levelNumer);
        var level = "L"+(levelNumer-1).ToString();
        return PlayerPrefs.GetInt(MainMenuController.Instance.GameController.EpisodeName + "." + level + "." + FINISHED, notfinished) == finished;
    }

    public void LevelFinishedData(string level)
    {
        PlayerPrefs.SetInt(MainMenuController.Instance.GameController.EpisodeName + "." + level + "." + FINISHED, finished);
    }

    public void SetCurrentEpisodeData(string key, string data, string level)
    {
        PlayerPrefs.SetString(MainMenuController.Instance.GameController.EpisodeName + "." + level + "." + key, data);
    }

    public void GetCurrentEpisodeData(string key, string level, out string result)
    {
        result = PlayerPrefs.GetString(MainMenuController.Instance.GameController.EpisodeName + "." + level + "." + key, "");
    }


    public void SetCurrentLevelData(string key, string data)
    {
        PlayerPrefs.SetString(MainMenuController.Instance.GameController.EpisodeName + "." + MainMenuController.Instance.GameController.LevelName + "." + key, data);
    }

    public void GetCurrentLevelData(string key, out string result)
    {
        result = PlayerPrefs.GetString(MainMenuController.Instance.GameController.EpisodeName + "." + MainMenuController.Instance.GameController.LevelName + "." + key, "");
    }
}
